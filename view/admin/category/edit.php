<?php

use App\Connection;
use App\HTML\Form;
use App\Table\CategoryTable;
use App\Validators\CategoryValidator;
use App\Auth;

Auth::check();

$pdo = Connection::getPDO();
$table = new CategoryTable($pdo);
$item = $table->find($params['id']);
$success = false;
$fields= ['name','slug'];
$errors = [];

if (!empty($_POST)){
    App\ObjectHelper::hydrate($item,$_POST,$fields);
    $validator = new CategoryValidator($_POST, $table, $item->getId());
    if ($validator->validate()){
        $table->update([
            'name' => $item->getName(),
            'slug' => $item->getSlug()
        ], $item->getId());
        $success = true;
    }else{
        $errors = $validator->errors();
    }
}

$form = new Form($item, $errors);
?>

<?php if ($success): ?>
<div class="alert alert-success">
    La catégorie à bien été modifié
</div>
<?php endif; ?>

<?php if (isset($_GET['created'])): ?>
<div class="alert alert-success">
    La catégorie à bien été créé
</div>
<?php endif; ?>

<?php if (!empty($errors)): ?>
<div class="alert alert-danger">
    La catégorie n'a pas pu être modifié, merci de corriger vos erreurs
</div>
<?php endif; ?>

<h1>Editer la catégorie -- <?= htmlentities($item->getName()) ?></h1>

<form action="" method="POST">
    <?= $form->input('name', 'Titre'); ?>
    <?= $form->input('slug', 'URL'); ?>
    <button class="btn btn-primary">Enregistrer</button>
</form>